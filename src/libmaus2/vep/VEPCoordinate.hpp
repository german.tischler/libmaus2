/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_VEP_VEPCOORDINATE_HPP)
#define LIBMAUS2_VEP_VEPCOORDINATE_HPP

#include <libmaus2/util/NumberSerialisation.hpp>

namespace libmaus2
{
	namespace vep
	{
		struct VEPCoordinate
		{
			int64_t contig;
			int64_t pos_0;
			int64_t pos_1;

			VEPCoordinate() {}
			VEPCoordinate(
				int64_t const rcontig,
				int64_t const rpos_0,
				int64_t const rpos_1
			) : contig(rcontig), pos_0(rpos_0), pos_1(rpos_1)
			{}

			bool operator<(VEPCoordinate const & V) const
			{
				if ( contig != V.contig )
					return contig < V.contig;
				else if( pos_0 != V.pos_0 )
					return pos_0 < V.pos_0;
				else if ( pos_1 != V.pos_1 )
					return pos_1 > V.pos_1;
				else
					return false;
			}

			std::ostream & serialise(std::ostream & out) const
			{
				libmaus2::util::NumberSerialisation::serialiseSignedNumber(out,contig);
				libmaus2::util::NumberSerialisation::serialiseSignedNumber(out,pos_0);
				libmaus2::util::NumberSerialisation::serialiseSignedNumber(out,pos_1);
				return out;
			}

			std::istream & deserialise(std::istream & in)
			{
				contig = libmaus2::util::NumberSerialisation::deserialiseSignedNumber(in);
				pos_0 = libmaus2::util::NumberSerialisation::deserialiseSignedNumber(in);
				pos_1 = libmaus2::util::NumberSerialisation::deserialiseSignedNumber(in);
				return in;
			}
		};
	}
}
#endif
