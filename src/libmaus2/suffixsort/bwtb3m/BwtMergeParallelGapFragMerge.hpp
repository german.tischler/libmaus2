/**
    libmaus2
    Copyright (C) 2009-2021 German Tischler-Höhle
    Copyright (C) 2011-2014 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#if ! defined(LIBMAUS2_SUFFIXSORT_BWTB3M_BWTMERGEPARALLELGAPFRAGMERGE_HPP)
#define LIBMAUS2_SUFFIXSORT_BWTB3M_BWTMERGEPARALLELGAPFRAGMERGE_HPP

#include <libmaus2/suffixsort/bwtb3m/BWTB3MBase.hpp>
#include <libmaus2/suffixsort/GapMergePacket.hpp>
#include <libmaus2/gamma/GammaGapDecoder.hpp>
#include <libmaus2/util/TempFileNameGenerator.hpp>

namespace libmaus2
{
	namespace suffixsort
	{
		namespace bwtb3m
		{
			struct BwtMergeParallelGapFragMerge : public BWTB3MBase
			{
				static uint64_t getRlLength(std::vector < std::vector < std::string > > const & bwtfilenames, uint64_t const numthreads);
				static std::vector<std::string> parallelGapFragMerge(
					libmaus2::util::TempFileNameGenerator & gtmpgen,
					std::vector < std::vector < std::string > > const & bwtfilenames,
					std::vector < std::vector < std::string > > const & gapfilenames,
					// std::string const & outputfilenameprefix,
					// std::string const & tempfilenameprefix,
					uint64_t const numthreads,
					uint64_t const lfblockmult,
					uint64_t const rlencoderblocksize,
					std::ostream * logstr,
					int const verbose
				);
			};
		}
	}
}
#endif
