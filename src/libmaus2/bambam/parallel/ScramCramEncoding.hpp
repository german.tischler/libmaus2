/*
    libmaus2
    Copyright (C) 2009-2021 German Tischler
    Copyright (C) 2011-2015 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_BAMBAM_PARALLEL_SCRAMCRAMENCODING_HPP)
#define LIBMAUS2_BAMBAM_PARALLEL_SCRAMCRAMENCODING_HPP

#include <libmaus2/exception/LibMausException.hpp>
#include <libmaus2/bambam/parallel/CramInterface.h>
#include <libmaus2/bambam/Scram.h>

namespace libmaus2
{
	namespace bambam
	{
		namespace parallel
		{
			struct ScramCramEncoding
			{
				typedef ScramCramEncoding this_type;
				typedef std::unique_ptr<this_type> unique_ptr_type;
				typedef std::shared_ptr<this_type> shared_ptr_type;

				static void * io_lib_cram_allocate_encoder(
					void *
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						userdata
						#endif
						,
					char const *
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						header
						#endif
						,
					size_t const
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						headerlength
						#endif
						,
					cram_data_write_function_t
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						writefunc
						#endif
				);

				static int io_lib_cram_set_profile(
					void *
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						context
						#endif
						,
					std::string const &
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						profile
						#endif
				);

				static void io_lib_cram_deallocate_encoder(
					void *
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						context
						#endif
				);

				static int io_lib_cram_enque_compression_block(
					void *
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						userdata
						#endif
						,
					void *
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						context
						#endif
						,
					size_t const
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						inblockid
						#endif
						,
					char const **
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						block
						#endif
						,
					size_t const *
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						blocksize
						#endif
						,
					size_t const *
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						blockelements
						#endif
						,
					size_t const
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						numblocks
						#endif
						,
					int const
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						final
						#endif
						,
					cram_enque_compression_work_package_function_t
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						workenqueuefunction
						#endif
						,
					cram_data_write_function_t
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						writefunction
						#endif
						,
					cram_compression_work_package_finished_t
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						workfinishedfunction
						#endif
				);

				static int io_lib_cram_process_work_package(
					void *
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						workpackage
						#endif
				);

				static int io_lib_cram_set_seqs_per_slice(
					void *
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						context
						#endif
						,
					size_t const
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						seqsperslice
						#endif
				);
				static int io_lib_cram_set_bases_per_slice(
					void *
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						context
						#endif
						,
					size_t const
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						basesperslice
						#endif
				);
				static int io_lib_cram_set_version(
					void *
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						context
						#endif
						,
					std::string const &
						#if defined(LIBMAUS2_HAVE_IO_LIB)
						version
						#endif
				);
			};
		}
	}
}
#endif
