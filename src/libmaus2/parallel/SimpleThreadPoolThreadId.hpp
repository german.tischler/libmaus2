/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_SIMPLETHREADPOOLTHREADID_HPP)
#define LIBMAUS2_PARALLEL_SIMPLETHREADPOOLTHREADID_HPP

#include <thread>
#include <string>
#include <sstream>
#include <libmaus2/types/types.hpp>
#include <libmaus2/parallel/SimpleThreadPoolBase.hpp>

namespace libmaus2
{
	namespace parallel
	{
		struct StdThread;

		struct SimpleThreadPoolThreadId
		{
			std::thread::id id;
			libmaus2::parallel::StdThread * thread;
			libmaus2::parallel::SimpleThreadPoolBase * pool;
			uint64_t index;

			SimpleThreadPoolThreadId()
			: id(), thread(nullptr), pool(nullptr), index(0)
			{

			}

			SimpleThreadPoolThreadId(
				std::thread::id const rid,
				libmaus2::parallel::StdThread * const rthread,
				libmaus2::parallel::SimpleThreadPoolBase * const rpool,
				uint64_t const rindex
			) : id(rid), thread(rthread), pool(rpool), index(rindex) {}

			bool operator<(SimpleThreadPoolThreadId const & O) const
			{
				return id < O.id;
			}

			std::string toString() const
			{
				std::ostringstream ostr;
				ostr << "(" << id << "," << thread << "," << pool << "," << index << ")";
				return ostr.str();
			}
		};
	}
}
#endif
