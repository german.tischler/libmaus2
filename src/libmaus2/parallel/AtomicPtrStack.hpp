/*
    libmaus2
    Copyright (C) 2021 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_ATOMICPTRSTACK_HPP)
#define LIBMAUS2_PARALLEL_ATOMICPTRSTACK_HPP

#include <libmaus2/util/atomic_shared_ptr.hpp>
#include <mutex>

namespace libmaus2
{
	namespace parallel
	{
		template<typename type>
		struct AtomicPtrStack
		{
			private:
			std::mutex lock;
			libmaus2::util::atomic_shared_ptr<
				libmaus2::util::atomic_shared_ptr<type>[]
			> p;
			std::atomic<std::size_t> f;
			std::atomic<std::size_t> n;

			static void swap(libmaus2::util::atomic_shared_ptr<type> * H, std::size_t const i, std::size_t const j)
			{
				libmaus2::util::shared_ptr<type> tmp = H[i].load();
				H[i].store(H[j].load());
				H[j].store(tmp);
			}

			template<typename comparator>
			static bool compare(libmaus2::util::atomic_shared_ptr<type> * H, std::size_t const i, std::size_t const j, comparator const & comp)
			{
				return comp(
					static_cast<type const &>(*(H[i].load())),
					static_cast<type const &>(*(H[j].load()))
				);
			}

			AtomicPtrStack<type> & operator=(AtomicPtrStack<type> const & O) = delete;

			void pushNoLock(libmaus2::util::shared_ptr<type> T)
			{
				if ( f.load() == n.load() )
				{
					std::size_t const old_n = n.load();
					std::size_t const new_n = old_n ? 2*old_n : 1;

					libmaus2::util::shared_ptr<
						libmaus2::util::atomic_shared_ptr<type>[]
					> nptr(new libmaus2::util::atomic_shared_ptr<type>[new_n]);

					libmaus2::util::atomic_shared_ptr<type> * tptr = nptr.get();
					libmaus2::util::atomic_shared_ptr<type> * optr = p.load().get();

					for ( std::size_t i = 0; i < old_n; ++i )
						tptr[i].store(optr[i].load());

					p.store(nptr);
					n.store(new_n);
				}

				(p.load().get())[f++].store(T);
			}

			bool popNoLock(libmaus2::util::shared_ptr<type> & T)
			{
				if ( ! f.load() )
					return false;

				T = p.load().get()[--f].load();

				return true;
			}

			public:
			AtomicPtrStack(std::size_t const rn = 0)
			: p(), f(0), n(rn)
			{
				if ( n )
				{
					libmaus2::util::shared_ptr<
						libmaus2::util::atomic_shared_ptr<type>[]
					> nptr(new libmaus2::util::atomic_shared_ptr<type>[n]);

					p.store(nptr);
				}
			}

			std::size_t size()
			{
				std::lock_guard<decltype(lock)> slock(lock);
				return f.load();
			}

			void push(libmaus2::util::shared_ptr<type> T)
			{
				std::lock_guard<decltype(lock)> slock(lock);
				pushNoLock(T);
			}

			bool pop(libmaus2::util::shared_ptr<type> & T)
			{
				std::lock_guard<decltype(lock)> slock(lock);
				return popNoLock(T);
			}

			libmaus2::util::shared_ptr<type> operator[](std::size_t const i)
			{
				std::lock_guard<decltype(lock)> slock(lock);
				return p.load().get()[i].load();
			}

			void set(std::size_t const i, libmaus2::util::shared_ptr<type> ptr)
			{
				std::lock_guard<decltype(lock)> slock(lock);
				return p.load().get()[i].store(ptr);
			}

			bool top(libmaus2::util::shared_ptr<type> & T) const
			{
				std::lock_guard<decltype(lock)> slock(lock);

				if ( f.load() )
				{
					T = p.load()[0].load();
					return true;
				}
				else
				{
					return false;
				}
			}


			template<typename comparator = std::less<type> >
			void pushHeap(libmaus2::util::shared_ptr<type> T, comparator const & comp = comparator())
			{
				std::lock_guard<decltype(lock)> slock(lock);

				size_t i = f.load();

				pushNoLock(T);

				libmaus2::util::atomic_shared_ptr<type> * H = p.load().get();

				// while not root
				while ( i )
				{
					// parent index
					size_t const p = (i-1)>>1;

					// order wrong?
					if ( compare(H,i,p,comp) )
					{
						swap(H,i,p);
						// std::swap(H[i],H[p]);
						i = p;
					}
					// order correct, break loop
					else
					{
						break;
					}
				}
			}

			template<typename comparator = std::less<type> >
			bool popHeap(libmaus2::util::shared_ptr<type> & T, comparator const & comp = comparator())
			{
				std::lock_guard<decltype(lock)> slock(lock);

				if ( ! f.load() )
					return false;

				libmaus2::util::atomic_shared_ptr<type> * H = p.load().get();

				T = H[0].load();

				// put last element at root
				H[0].store(H[--f].load());

				size_t i = 0;
				size_t r;

				// while both children exist
				while ( (r=((i<<1)+2)) < f )
				{
					size_t const m = compare(H,r-1,r,comp) ? r-1 : r;

					// order correct?
					if ( compare(H,i,m,comp) )
						return true;

					swap(H,i,m);
					// std::swap(H[i],H[m]);
					i = m;
				}

				// does left child exist?
				size_t l;
				if ( ((l = ((i<<1)+1)) < f) && (!(compare(H,i,l,comp))) )
					swap(H,i,l);
					// std::swap(H[i],H[l]);

				return true;
			}
		};
	}
}
#endif
