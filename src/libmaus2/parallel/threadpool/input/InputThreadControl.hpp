/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_INPUT_INPUTTHREADCONTROL_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_INPUT_INPUTTHREADCONTROL_HPP

#include <libmaus2/parallel/threadpool/ThreadPool.hpp>
#include <libmaus2/parallel/threadpool/input/InputThreadPackageDispatcher.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace input
			{
				struct InputThreadControl
				{
					libmaus2::parallel::threadpool::ThreadPool & TP;
					std::vector< std::shared_ptr<std::istream> > Vstr;
					std::vector<ThreadPoolInputInfo::shared_ptr_type> Vin;
					uint64_t const numstreams;
					uint64_t const numblocks;
					std::vector< std::shared_ptr< std::atomic<int> > > VstreamFinished;
					std::atomic<uint64_t> streamsFinished;
					std::vector<InputThreadCallbackInterface *> Viface;
					uint64_t const basepriority;
					uint64_t const modpriority;

					void registerDispatcher()
					{
						TP.registerDispatcher<libmaus2::parallel::threadpool::input::InputThreadPackageData>(
							libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::ThreadPoolDispatcher>(
								new libmaus2::parallel::threadpool::input::InputThreadPackageDispatcher
							)
						);
					}


					InputThreadControl(
						libmaus2::parallel::threadpool::ThreadPool & rTP,
						std::vector< std::shared_ptr<std::istream> > rVstr,
						uint64_t const rnumblocks,
						uint64_t const blocksize,
						uint64_t const putbacksize,
						uint64_t const rbasepriority,
						uint64_t const rmodpriority
					)
					: TP(rTP), Vstr(rVstr), Vin(Vstr.size()), numstreams(Vin.size()), numblocks(rnumblocks), VstreamFinished(Vstr.size()), streamsFinished(0), Viface(), basepriority(rbasepriority), modpriority(rmodpriority)
					{
						registerDispatcher();

						for ( uint64_t i = 0; i < Vstr.size(); ++i )
						{
							ThreadPoolInputInfo::shared_ptr_type ptr(new ThreadPoolInputInfo(this,*Vstr[i],i,numblocks,blocksize,putbacksize));
							Vin[i] = ptr;
							VstreamFinished[i] = std::shared_ptr< std::atomic<int> >(new std::atomic<int>(0));
						}
					}

					InputThreadControl(
						libmaus2::parallel::threadpool::ThreadPool & rTP,
						std::vector<std::string> Vfn,
						uint64_t const rnumblocks,
						uint64_t const blocksize,
						uint64_t const putbacksize,
						uint64_t const rbasepriority,
						uint64_t const rmodpriority
					)
					: TP(rTP), Vstr(), Vin(), numstreams(Vfn.size()), numblocks(rnumblocks), VstreamFinished(Vfn.size()), streamsFinished(0), Viface(), basepriority(rbasepriority), modpriority(rmodpriority)
					{
						registerDispatcher();

						for ( uint64_t i = 0; i < Vfn.size(); ++i )
						{
							ThreadPoolInputInfo::shared_ptr_type ptr(new ThreadPoolInputInfo(this,Vfn[i],i,numblocks,blocksize,putbacksize));
							Vin.push_back(ptr);
							VstreamFinished[i] = std::shared_ptr< std::atomic<int> >(new std::atomic<int>(0));
						}
					}

					void setCallback(
						std::vector<InputThreadCallbackInterface *> rViface
					)
					{
						Viface = rViface;
					}

					bool readFinished()
					{
						return streamsFinished.load() == numstreams;
					}

					bool readFinished(uint64_t const streamid)
					{
						return VstreamFinished[streamid]->load();
					}

					void start()
					{
						for ( uint64_t i = 0; i < Vin.size(); ++i )
						{
							libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::ThreadWorkPackage> pack(TP.getPackage<InputThreadPackageData>());
							InputThreadPackageData & data = dynamic_cast<InputThreadPackageData &>(*(pack->data.load()));
							data.inputinfo = Vin[i].get();
							data.callback = Viface[i];
							pack->prio = basepriority + ::std::rand()%modpriority;
							TP.enqueue(pack);
						}
					}

					void returnBlock(libmaus2::util::shared_ptr<ThreadPoolInputBlock> block)
					{
						ThreadPoolInputInfo * const inputinfo = block->getInputInfo();
						bool const eof = block->eof.load();
						inputinfo->returnedBlocks.push(block);

						if ( eof )
						{
							++streamsFinished;
							VstreamFinished[inputinfo->streamid]->store(1);
						}
						else
						{
							libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::ThreadWorkPackage> pack(TP.getPackage<InputThreadPackageData>());
							InputThreadPackageData & data = dynamic_cast<InputThreadPackageData &>(*(pack->data.load()));
							data.inputinfo = inputinfo;
							data.callback = Viface[inputinfo->streamid];
							pack->prio = basepriority + ::std::rand()%modpriority;
							TP.enqueue(pack);
						}
					}
				};

			}
		}
	}
}
#endif
