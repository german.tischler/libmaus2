/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_INPUT_INPUTTHREADPACKAGEDISPATCHER_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_INPUT_INPUTTHREADPACKAGEDISPATCHER_HPP

#include <libmaus2/parallel/threadpool/ThreadPoolDispatcher.hpp>
#include <libmaus2/parallel/threadpool/input/InputThreadPackageData.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace input
			{
				struct InputThreadPackageDispatcher : public libmaus2::parallel::threadpool::ThreadPoolDispatcher
				{
					virtual void dispatch(
						libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::ThreadWorkPackage> package,
						libmaus2::parallel::threadpool::ThreadPool * /* pool */
					)
					{
						InputThreadPackageData & HP = dynamic_cast<InputThreadPackageData &>(*(package->data.load()));
						ThreadPoolInputInfo & inputinfo = *(HP.inputinfo);
						InputThreadCallbackInterface & callback = *(HP.callback);

						uint64_t pushed = 0;

						// if we can get the lock
						if ( inputinfo.lock.try_lock() )
						{
							// adopt the lock in unique_lock
							std::unique_lock<std::mutex> slock(inputinfo.lock,std::adopt_lock_t());

							// put returned blocks into free list
							libmaus2::util::shared_ptr<ThreadPoolInputBlock> ptr;
							while ( inputinfo.returnedBlocks.pop(ptr) )
							{
								inputinfo.freeBlocks.push(ptr);
								++inputinfo.blocksFree;
							}

							// get number of free blocks
							uint64_t const lnumfree = inputinfo.blocksFree.load();

							// if number of free blocks is at least blocksMinFree
							if ( lnumfree >= inputinfo.blocksMinFree )
							{
								// set time before getting first block
								if ( ! inputinfo.rtcSet.load() )
								{
									inputinfo.rtc.start();
									inputinfo.rtcSet.store(true);
								}

								// read while we have free blocks and not received end of file
								while ( inputinfo.freeBlocks.pop(ptr) && !inputinfo.eof.load() )
								{
									// decrement number of free blocks
									--inputinfo.blocksFree;

									// get pointer to start of data
									char * const ca = ptr->A.begin()+ptr->putback;
									// read block
									inputinfo.in.read(ca,ptr->blocksize);
									// get number of bytes we read
									std::size_t const n = inputinfo.in.gcount();
									// set end of data pointer
									char * const ce = ca + n;

									// set data in block structure
									ptr->ca = ca;
									ptr->ce = ce;
									ptr->blockid = inputinfo.blocksRead++;
									ptr->setInputInfo(&inputinfo);

									// increase total number of bytes read so far
									inputinfo.bytesRead += n;

									// check whether we reached the end of the file
									bool const eof = inputinfo.in.peek() == std::istream::traits_type::eof();

									// store eof if we have reached it
									if ( eof )
										inputinfo.eof.store(true);
									ptr->eof.store(eof);

									assert ( HP.callback );

									// put block in output queue
									inputinfo.outQueue.push(ptr);

									// increment number of blocks read
									++pushed;
								}
							}
						}

						// invoke callback if we have read any blocks
						if ( pushed )
							callback.inputThreadCallbackInterfaceBlockRead(HP.inputinfo);
					}
				};
			}
		}
	}
}
#endif
