/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFDECOMPRESSEDBLOCK_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFDECOMPRESSEDBLOCK_HPP

#include <libmaus2/parallel/threadpool/bgzf/GenericBlock.hpp>
#include <libmaus2/parallel/threadpool/bgzf/BgzfBlockInfo.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bgzf
			{
				struct BgzfDecompressedBlock
				{
					std::atomic<uint64_t> streamid;
					std::atomic<uint64_t> blockid;
					std::atomic<uint64_t> subid;
					std::atomic<uint64_t> absid;
					libmaus2::util::atomic_shared_ptr<GenericBlock> ddata;
					std::atomic<uint64_t> n;
					libmaus2::util::atomic_shared_ptr<BgzfBlockInfo> blockinfo;

					public:
					BgzfDecompressedBlock()
					: streamid(0), blockid(0), subid(0), absid(0), ddata(), n(0), blockinfo()
					{}
					BgzfDecompressedBlock(
						uint64_t const rstreamid,
						uint64_t const rblockid,
						uint64_t const rsubid,
						uint64_t const rabsid,
						libmaus2::util::shared_ptr<GenericBlock> rddata,
						uint64_t const rn,
						libmaus2::util::shared_ptr<BgzfBlockInfo> rblockinfo
					) : streamid(rstreamid), blockid(rblockid), subid(rsubid), absid(rabsid), ddata(rddata), n(rn), blockinfo(rblockinfo)
					{

					}
					BgzfDecompressedBlock(BgzfDecompressedBlock const & O)
					: streamid(O.streamid.load()), blockid(O.blockid.load()), subid(O.subid.load()), absid(O.absid.load()), ddata(O.ddata.load()), n(O.n.load()), blockinfo(O.blockinfo.load()) {}

					BgzfDecompressedBlock & operator=(BgzfDecompressedBlock const & O)
					{
						if ( this != &O )
						{
							streamid.store(O.streamid.load());
							blockid.store(O.blockid.load());
							subid.store(O.subid.load());
							absid.store(O.absid.load());
							ddata.store(O.ddata.load());
							n.store(O.n.load());
							blockinfo.store(O.blockinfo.load());
						}
						return *this;
					}

					bool operator<(BgzfDecompressedBlock const & O) const
					{
						return absid.load() < O.absid.load();
					}
				};
			}
		}
	}
}
#endif

