/*
    libmaus2
    Copyright (C) 2009-2013 German Tischler
    Copyright (C) 2011-2013 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#if ! defined(LIBMAUS2_UTIL_UNORDERED_MAP_HPP)
#define LIBMAUS2_UTIL_UNORDERED_MAP_HPP

#include <libmaus2/LibMausConfig.hpp>

#include <unordered_map>

namespace libmaus2
{
	namespace util
	{
		template<typename T>
		struct unordered_map_hash
		{
			typedef std::hash<T> hash_type;
		};

		template<typename T1, typename T2, typename H = typename unordered_map_hash<T1>::hash_type >
		struct unordered_map
		{
			typedef typename ::std::unordered_map<T1,T2,H> type;
		};
	}
}
#endif
