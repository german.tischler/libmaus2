/*
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_BITIO_BITSET_HPP)
#define LIBMAUS2_BITIO_BITSET_HPP

#include <libmaus2/bitio/BitVector.hpp>

namespace libmaus2
{
	namespace bitio
	{
		struct BitSet
		{
			typedef BitSet this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;

			libmaus2::bitio::BitVector BV;
			libmaus2::autoarray::AutoArray<uint64_t> A;
			uint64_t f;

			BitSet(uint64_t const rn) : BV(rn), A(0), f(0)
			{

			}

			void set(uint64_t const i)
			{
				if ( !BV.get(i) )
				{
					BV.set(i);
					A.push(f,i);
				}
			}

			bool get(uint64_t const i) const
			{
				return BV.get(i);
			}

			void clear()
			{
				while ( f )
					BV.erase(A[--f]);
			}

			void sort()
			{
				std::sort(
					A.begin(),A.begin()+f
				);
			}
		};
	}
}
#endif
