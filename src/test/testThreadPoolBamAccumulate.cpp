/*
    libmaus2
    Copyright (C) 2021 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/parallel/threadpool/bam/ThreadPoolBamParseAccumulateHandler.hpp>
#include <libmaus2/parallel/NumCpus.hpp>
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/aio/StreamLock.hpp>
#include <libmaus2/util/MemoryStatistics.hpp>

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser arg(argc,argv);

		std::string const fn = "tmp.fn";
		std::size_t const inputblocks = 64;
		std::size_t const inputblocksize = 1024*1024;
		std::size_t const hwmem = libmaus2::util::MemoryStatistics::getPhysicalMemory();
		std::cerr << "physical memory " << hwmem << std::endl;
		std::size_t const target_size = hwmem/4;
		std::size_t const num_buffers = 4;
		std::size_t const targetbuffersize = (target_size + num_buffers - 1) / num_buffers; // 8*1024ull*1024ull*1024ull;
		std::size_t const bgzfsize = 64*1024;
		std::size_t const numdecompressedblocks = target_size / bgzfsize;
		std::size_t const threads = libmaus2::parallel::NumCpus::getNumLogicalProcessors() / 2;

		libmaus2::parallel::threadpool::ThreadPool TP(threads);
		libmaus2::parallel::threadpool::bam::BamSortFunctionsQueryName BSFQN;
		libmaus2::parallel::threadpool::bam::ThreadPoolBamParseAccumulateHandler TPBPTRH(BSFQN,fn,targetbuffersize,num_buffers);

		std::unique_ptr<libmaus2::parallel::threadpool::bam::ThreadPoolBamParseControl> pTPBDC;

		std::vector<std::string> Vfn;
		for ( uint64_t i = 0; i < arg.size(); ++i )
			Vfn.push_back(arg[i]);


		if ( Vfn.size() )
		{

			std::unique_ptr<libmaus2::parallel::threadpool::bam::ThreadPoolBamParseControl>
				tptr(new libmaus2::parallel::threadpool::bam::ThreadPoolBamParseControl(TP,Vfn,&TPBPTRH,inputblocks,inputblocksize,numdecompressedblocks)
			);

			pTPBDC = std::move(tptr);
		}
		else
		{
			std::shared_ptr<std::istream> ptr(&std::cin, []([[maybe_unused]]auto p){});

			std::unique_ptr<libmaus2::parallel::threadpool::bam::ThreadPoolBamParseControl>
				tptr(new libmaus2::parallel::threadpool::bam::ThreadPoolBamParseControl(TP,std::vector< std::shared_ptr<std::istream> >(1,ptr),&TPBPTRH,inputblocks,inputblocksize,numdecompressedblocks)
			);

			pTPBDC = std::move(tptr);
		}

		pTPBDC->start();

		TP.join();

		std::cerr << "[V] finished and joined" << std::endl;

		TPBPTRH.flushStreams();
		TPBPTRH.closeStreams();

		std::cerr << "[V] flushed and close streams" << std::endl;

		std::pair<std::string,std::string> const P_fn(TPBPTRH.getFileNames(0));

		libmaus2::parallel::threadpool::bam::IndexEntrySet IES(P_fn.second);
		std::size_t const index_n = IES.size();
		std::size_t const index_per_thread = (index_n + threads - 1)/threads;

		for ( std::size_t t = 0; t <= threads; ++t )
		{
			std::size_t const s = std::min(t * index_per_thread, index_n);

			auto const V_split = IES.getSplit(BSFQN,s);

			for ( std::size_t i = 0; i < V_split.size(); ++i )
			{
				char const * record = V_split[i]->record;
				uint8_t const * u_record = reinterpret_cast<uint8_t const *>(record);
				std::cerr << t << "/" << i << "=" << (V_split[i] - IES.V_split[i].first) << "/" << IES.V_entry[i]->size() << "\t";

				if ( V_split[i] != IES.V_split[i].second )
					std::cerr << libmaus2::bambam::BamAlignmentDecoderBase::getReadName(u_record + sizeof(uint32_t));
				else
					std::cerr << "*";

				std::cerr << std::endl;
			}
		}
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
